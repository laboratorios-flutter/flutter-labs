

import 'dart:convert';

ResponseAlumnos ResponseAlumnosFromJson(String str) => ResponseAlumnos.fromJson(json.decode(str));

String ResponseAlumnosToJson(ResponseAlumnos data) => json.encode(data.toJson());

class ResponseAlumnos {
    String status;
    String message;
    List<Alumno> data;

    ResponseAlumnos({
        required this.status,
        required this.message,
        required this.data,
    });

    factory ResponseAlumnos.fromJson(Map<String, dynamic> json) => ResponseAlumnos(
        status: json["status"],
        message: json["message"],
        data: List<Alumno>.from(json["data"].map((x) => Alumno.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Alumno {
    String id;
    String nombreCompleto;
    String email;
    String docIdentidad;
    String fechaNacimiento;
    String imagen;

    Alumno({
        required this.id,
        required this.nombreCompleto,
        required this.email,
        required this.docIdentidad,
        required this.fechaNacimiento,
        required this.imagen,
    });

    factory Alumno.fromJson(Map<String, dynamic> json) => Alumno(
        id: json["_id"],
        nombreCompleto: json["nombre_completo"],
        email: json["email"],
        docIdentidad: json["doc_identidad"],
        fechaNacimiento: json["fecha_nacimiento"],
        imagen: json["imagen"],
    );

    Map<String, dynamic> toJson() => {
        "_id": id,
        "nombre_completo": nombreCompleto,
        "email": email,
        "doc_identidad": docIdentidad,
        "fecha_nacimiento": fechaNacimiento,
        "imagen": imagen,
    };

}
