import 'package:flutter/material.dart';
import 'package:proyecto_base/screens/sidebar_menu.dart';
import 'package:proyecto_base/alumnos/services/alumno.service.dart';
import 'package:proyecto_base/alumnos/models/alumno.model.dart';
import 'package:proyecto_base/alumnos/alumno_view.dart';

class AlumnosList2 extends StatefulWidget {
  const AlumnosList2({super.key});

  @override
  State<AlumnosList2> createState() => _AlumnosListState();
}

class _AlumnosListState extends State<AlumnosList2> {
  List<Alumno> _alumnos = [];

  void _loadData() async{
    var list =  await ApiService.getAlumnos();
    setState(() {
      _alumnos = list;
    }); 
  }

  @override
  void initState()  {
    super.initState();
      _loadData();
    final now = DateTime.now();
    print('initState $now ');
  }

  @override
  void dispose() {
    setState(() {
      _alumnos.clear();
    });
    
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Gestión - Alumnos 2'),
        ),
        drawer: SidebarMenu(),
        body: ListView.builder(
                itemCount: _alumnos.length,
                itemBuilder: (context, index) {
                  final Alumno alumno = _alumnos[index];
                  return Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: GestureDetector(
                        child: Card(
                          child: ListTile(
                            leading: Icon(Icons.person),
                            title: Text(alumno.nombreCompleto),
                            subtitle: Text(alumno.id),
                          ),
                        ),
                        onTap: ()=> Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (BuildContext context)=> AlumnoView(alumno: alumno)
                          ),
                        ),
                      )
                    );
                },
            ),
    );
  }
}
