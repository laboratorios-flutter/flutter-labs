import 'dart:io';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:camera/camera.dart';
import 'package:http/http.dart' as http;
import 'package:proyecto_base/global.dart';
import 'package:proyecto_base/alumnos/models/alumno.model.dart';

class ApiService {
  static const String baseUri = URL_ALUMNOS;

  static Future<List<Alumno>> getAlumnos() async {
    final response = await http.get(Uri.parse(baseUri));
    if (response.statusCode == 200) {
      final List<dynamic> jsonList = json.decode(response.body)['data'];
      return jsonList.map((el) => Alumno.fromJson(el)).toList();
    } else {
      throw Exception('Failed to fetch Alumnos');
    }
  }

  static Future<Alumno> getAlumno(String alumnoId) async {
    final url = '$baseUri/${alumnoId}';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      //final dynamic json = jsonDecode(response.body);
      //return Alumno.fromJson(json);
      final dynamic json = jsonDecode(response.body)['data'][0];
      return Alumno.fromJson(json);
    } else {
      throw Exception('Failed to fetch Alumnos');
    }
  }

  static Future<String> createAlumno(Alumno alumno) async {
    final response = await http.post(Uri.parse(baseUri),
        headers: {'Content-Type': 'application/json'},
        body: json.encode(alumno.toJson()));
    if (response.statusCode == 200) {
      return jsonDecode(response.body)['status'];
    } else {
      throw Exception('Failed to fetch Alumno');
    }
  }

  static Future<String> updateAlumno(Alumno alumno) async {
    final url = '$baseUri/${alumno.id}';
    print(json.encode(alumno.toJson()));
    print(url);
    final response = await http.put(Uri.parse(url),
        headers: {'Content-Type': 'application/json'},
        body: json.encode(alumno.toJson()));
    if (response.statusCode == 200) {
      print(response.body);
      return jsonDecode(response.body)['status'];
    } else {
      throw Exception('Failed to fetch Alumno');
    }
  }

  static Future<void> uploadImage(XFile image) async {

    try {

    final url = '$baseUri/upload';

      // Create a multipart request for the API endpoint
    var request = http.MultipartRequest(
        'POST',
        Uri.parse(url),
    );

    // Create a file object from the captured image path
    var imageFile = File(image.path);

    // Add the image file to the request
    request.files.add(await http.MultipartFile.fromPath('image', imageFile.path));

    // Send the request and get the response
    var response = await request.send();

    if (response.statusCode == 200) {
        // Successful upload
        print('Image uploaded successfully');
      } else {
        // Failed upload
        print('Image upload failed');
    }


    } catch (ex) {
      print('ERROR UPLOAD IMAGEN : $ex '); 
    }

  }

  static Future<String> deleteAlumno(String alumnoId) async {
    final url = '$baseUri/${alumnoId}';
    final response = await http.delete(Uri.parse(url));
    if (response.statusCode == 200) {
      return jsonDecode(response.body)['status'];
    } else {
      throw Exception('Failed to fetch Alumno');
    }
  }

static Future<void> uploadImage2(XFile image) async {
  final url = '$baseUri/upload';
  try {
    Dio dio = Dio();

    FormData formData = FormData.fromMap({
      'image': await MultipartFile.fromFile(image.path, filename: 'test.jpg'),
    });

    Response response = await dio.post(url, data: formData);

    if (response.statusCode == 200) {
      print('Image uploaded successfully dio');
    } else {
      print('Image upload failed');
    }
  } catch (e) {
    print('Error Dio : $e');
  }
}



}
