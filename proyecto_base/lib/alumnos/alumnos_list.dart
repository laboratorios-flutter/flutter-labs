import 'package:flutter/material.dart';
import 'package:proyecto_base/screens/sidebar_menu.dart';
import 'package:proyecto_base/alumnos/services/alumno.service.dart';
import 'package:proyecto_base/alumnos/models/alumno.model.dart';
import 'package:proyecto_base/alumnos/alumno_view.dart';
import 'package:proyecto_base/alumnos/alumno_add.dart';

class AlumnosList extends StatefulWidget {
  const AlumnosList({super.key});

  @override
  State<AlumnosList> createState() => _AlumnosListState();
}

class _AlumnosListState extends State<AlumnosList> {
  late Future<List<Alumno>> _alumnos;
  late List<Alumno> _alumnoList;

  @override
  void initState() {
    super.initState();
    _alumnos = ApiService.getAlumnos();
    _alumnoList = [];
  }

  @override
  void dispose() {
    super.dispose();
  }

  void navigateToChildWidget(Alumno alumno) async {

    //Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => AlumnoView(alumno: alumno)));

    final result = await Navigator.push(context,MaterialPageRoute(builder: (context) => AlumnoView(alumno: alumno)));

    if(result != null){
      print('resultado al retornar : $result');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gestión - Alumnos'),
      ),
      drawer: SidebarMenu(),
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {
            _alumnos = ApiService.getAlumnos();
          });
        },
        child: FutureBuilder(
        future: _alumnos,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            print(snapshot.data);
            List<Alumno> alumnos = snapshot.data;
            _alumnoList = snapshot.data;
            return ListView.builder(
              itemCount: _alumnoList.length,
              itemBuilder: (context, index) {
                final Alumno alumno = _alumnoList[index];
                return Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: GestureDetector(
                        child: Card(
                          child: ListTile(
                            leading: Icon(Icons.person),
                            title: Text(alumno.nombreCompleto),
                            subtitle: Text(alumno.id),
                          ),
                        ),
                        onTap: () => {
                              navigateToChildWidget(alumno)
                        }
                  ));
              },
            );
          }
        },
      ),
      ),
      
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.person_add),
        onPressed: () async {
          //Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {return AlumnoAdd();},)),
          final Alumno alumno = await Navigator.push(
            context,
            MaterialPageRoute(builder: (context)=> AlumnoAdd())
          );
          if(alumno != null){
            setState(() {
              _alumnoList.add(alumno);
            });
          }
        }
      ),
    );
  }
}
