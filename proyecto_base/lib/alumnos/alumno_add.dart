import 'package:flutter/material.dart';
import 'package:proyecto_base/alumnos/models/alumno.model.dart';
import 'package:proyecto_base/alumnos/services/alumno.service.dart';

class AlumnoAdd extends StatefulWidget {
  const AlumnoAdd({super.key});

  @override
  State<AlumnoAdd> createState() => _AlumnoAddState();
}

class _AlumnoAddState extends State<AlumnoAdd> {

  final _formKey = GlobalKey<FormState>();
  late TextEditingController _nombre_completo;
  late TextEditingController _email;
  late TextEditingController _doc_identidad;

  @override
  void initState() {
    super.initState();
    _nombre_completo = TextEditingController();
    _email = TextEditingController();
    _doc_identidad = TextEditingController();
  }

  @override
  void dispose() {
    _nombre_completo.dispose();
    _email.dispose();
    _doc_identidad.dispose();
    super.dispose();
  }

    void _saveAlumno() {
    if (_formKey.currentState!.validate()) {
      final newAlumno = Alumno(
        id : '',
        nombreCompleto:_nombre_completo.text,
        email: _email.text,
        docIdentidad: _doc_identidad.text,
        fechaNacimiento:'',
        imagen:''
      );

      ApiService.createAlumno(newAlumno);

      // Clear the form fields
      _nombre_completo.clear();
      _email.clear();
      _doc_identidad.clear();

      // Show a snackbar or navigate to another screen
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Alumno added successfully')),
      );
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Alumnos'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                controller: _nombre_completo,
                decoration: InputDecoration(labelText: 'Nombre Completo'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter Nombre Completo';
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: _email,
                decoration: InputDecoration(labelText: 'Email'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter email';
                  }
                  return null;
                },
              ),
              TextFormField(
                controller: _doc_identidad,
                decoration: InputDecoration(labelText: 'Doc.Identidad'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter Doc Identidad';
                  }
                  return null;
                },
              ),
              SizedBox(height: 16.0),
              ElevatedButton(
                onPressed: _saveAlumno,
                child: Text('Save Alumno'),
              ),
            ],
          ),
        ),
      ),
    );



  }
}