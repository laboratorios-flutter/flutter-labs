import 'package:flutter/material.dart';
import 'package:proyecto_base/alumnos/models/alumno.model.dart';
import 'package:proyecto_base/alumnos/services/alumno.service.dart';


class AlumnoView extends StatefulWidget {
  final Alumno alumno;

  const AlumnoView({Key?key,required this.alumno}):super(key: key);

  @override
  State<AlumnoView> createState() => _AlumnoViewState();
}

class _AlumnoViewState extends State<AlumnoView> {

  late TextEditingController _nombre_completo;
  late TextEditingController _email;
  late TextEditingController _doc_identidad;
  bool _isEditing = false;

  @override
  void initState() {
    super.initState();
    _nombre_completo = TextEditingController(text: widget.alumno.nombreCompleto);
    _email = TextEditingController(text: widget.alumno.email);
    _doc_identidad = TextEditingController(text: widget.alumno.docIdentidad);
  }

  @override
  void dispose() {
    _nombre_completo.dispose();
    _email.dispose();
    _doc_identidad.dispose();
    super.dispose();
  }

  void _saveChanges(){
    final updateAlumno = Alumno(
      id: widget.alumno.id, 
      nombreCompleto: _nombre_completo.text, 
      email: _email.text, 
      docIdentidad: _doc_identidad.text, 
      fechaNacimiento: '', 
      imagen: ''
    );
    ApiService.updateAlumno(updateAlumno);
    _toggleEdit();
  }

  void _toggleEdit() {
    setState(() {
      _isEditing = !_isEditing;
    });
  }


  void _alertDelete(BuildContext context){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Confirmacion"),
          content: Text('Desea eliminar registro?') ,
          actions: [
            TextButton(onPressed: (){
              Navigator.of(context).pop(false);
              ApiService.deleteAlumno(widget.alumno.id);
              Navigator.pop(context,'delete : ${widget.alumno.nombreCompleto} ');
            }, child: Text('SI')),
            TextButton(onPressed: (){
              Navigator.of(context).pop(false); 
            }, child: Text('NO'))
          ],
        );
      }
    );
  } 


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Alumno ID : ${widget.alumno.id} '),
      ),
      body: Container(
        height: 250.0,
        padding: const EdgeInsets.all(20.0),
        child: FutureBuilder(
          future: ApiService.getAlumno(widget.alumno.id),
          builder: (BuildContext context,AsyncSnapshot snapshot){
            if(snapshot.connectionState == ConnectionState.waiting){
              return Center(child: CircularProgressIndicator(),);
            }else{
              Alumno alumnoS = snapshot.data;
              return Card(
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextField(
                        controller: _nombre_completo,
                        enabled: _isEditing,
                        decoration: InputDecoration(labelText: 'Nombre completo'),
                      ),
                      TextField(
                        controller: _email,
                        enabled: _isEditing,
                        decoration: InputDecoration(labelText: 'Email'),
                      ),
                      TextField(
                        controller: _doc_identidad,
                        enabled: _isEditing,
                        decoration: InputDecoration(labelText: 'Doc.Identidad'),
                      ),
                      SizedBox(height: 16.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          if (_isEditing) ElevatedButton.icon(onPressed: _saveChanges, icon: Icon(Icons.save),label: Text('Save'), ),
                          if (_isEditing) ElevatedButton(onPressed: _toggleEdit, child: Text('Cancel')),
                          if (!_isEditing) ElevatedButton.icon(onPressed: _toggleEdit,icon: Icon(Icons.edit),label: Text('Edit'),),
                          if (!_isEditing)
                            ElevatedButton.icon(
                              onPressed: (){
                                _alertDelete(context);
                              },
                              icon: Icon(Icons.delete),
                              label: Text('Delete'),
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
                              ), 
   
                            ),

                        ],
                      ),
                    ],
                  ),
                ),
              );
            }
          } ,
        )
      ),
    );
  }
}