import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DBHelper {
  static Future<Database> database() async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, 'tasks.db');

    return openDatabase(
      path,
      version: 1,
      onCreate: (db, version) {
        return db.execute(
          'CREATE TABLE tasks(id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, completed INTEGER)',
        );
      },
    );
  }

  static Future<void> insertTask(Task task) async {
    final db = await DBHelper.database();
    await db.insert('tasks', task.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  static Future<List<Task>> getTasks() async {
    final db = await DBHelper.database();
    final List<Map<String, dynamic>> taskMapList = await db.query('tasks');
    return taskMapList.map((taskMap) => Task.fromMap(taskMap)).toList();
  }

  static Future<void> updateTask(Task task) async {
    final db = await DBHelper.database();
    await db.update('tasks', task.toMap(),
        where: 'id = ?', whereArgs: [task.id]);
  }

  static Future<void> deleteTask(int id) async {
    final db = await DBHelper.database();
    await db.delete('tasks', where: 'id = ?', whereArgs: [id]);
  }
}

class Task {
  final int? id;
  final String title;
  late final bool completed;

  Task({this.id,required this.title,required this.completed});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'completed': completed ? 1 : 0,
    };
  }

  factory Task.fromMap(Map<String, dynamic> map) {
    return Task(
      id: map['id'],
      title: map['title'],
      completed: map['completed'] == 1,
    );
  }
}
