import 'package:flutter/material.dart';
import 'dart:async';
import 'package:proyecto_base/sqlite/db_helper.dart';
import 'package:proyecto_base/screens/sidebar_menu.dart';

class TaskListScreen extends StatefulWidget {
  const TaskListScreen({super.key});

  @override
  State<TaskListScreen> createState() => _TaskListScreenState();
}

class _TaskListScreenState extends State<TaskListScreen> {
   List<Task> _tasks = [];

  @override
  void initState() {
    super.initState();
    _loadTasks();
  }

  Future<void> _loadTasks() async {
    final tasks = await DBHelper.getTasks();
    setState(() {
      _tasks = tasks;
    });
  }

  Future<void> _toggleTaskCompletion(Task task) async {
    task.completed = !task.completed;
    await DBHelper.updateTask(task);
    _loadTasks();
  }

  Future<void> _deleteTask(Task task) async {
    await DBHelper.deleteTask(task.id!);
    _loadTasks();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Task Manager'),
      ),
      drawer: SidebarMenu(),
      body: ListView.builder(
        itemCount: _tasks.length,
        itemBuilder: (ctx, index) {
          final task = _tasks[index];
          return ListTile(
            title: Text(task.title),
            trailing: Checkbox(
              value: task.completed,
              onChanged: (_) => _toggleTaskCompletion(task),
            ),
            onLongPress: () => _deleteTask(task),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          final newTask = await showDialog<Task>(
            context: context,
            builder: (ctx) => NewTaskDialog(),
          );
          if (newTask != null) {
            await DBHelper.insertTask(newTask);
            _loadTasks();
          }
        },
      ),
    );
  }

}

class NewTaskDialog extends StatefulWidget {
  @override
  _NewTaskDialogState createState() => _NewTaskDialogState();
}

class _NewTaskDialogState extends State<NewTaskDialog> {
  final _taskController = TextEditingController();

  @override
  void dispose() {
    _taskController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('New Task'),
      content: TextField(
        controller: _taskController,
        decoration: InputDecoration(
          labelText: 'Task Name',
        ),
      ),
      actions: [
        TextButton(
          child: Text('Cancel'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: Text('Add'),
          onPressed: () {
            final taskName = _taskController.text;
            if (taskName.isNotEmpty) {
              final newTask = Task(title: taskName, completed: false);
              Navigator.of(context).pop(newTask);
            }
          },
        ),
      ],
    );
  }
}