import 'package:flutter/material.dart';

class Usuario with ChangeNotifier {
  String usuario = '';

  String get getUsuario => usuario;

  void setUsuario(String value){
    usuario = value;
    notifyListeners();
  }

}