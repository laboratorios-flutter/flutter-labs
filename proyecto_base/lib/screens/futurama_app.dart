import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:proyecto_base/screens/sidebar_menu.dart';

void main() {
  runApp(FuturamaApp());
}

class FuturamaApp extends StatefulWidget {
  @override
  _FuturamaAppState createState() => _FuturamaAppState();
}

class _FuturamaAppState extends State<FuturamaApp> {
  //List<dynamic> characters = [];

  @override
  void initState() {
    super.initState();
    //fetchCharacters();
  }

  Future<List> _fetchFuturama() async {
    final resp = await http.get(Uri.parse('https://api.sampleapis.com/futurama/characters'));
    if (resp.statusCode == 200) {
      return json.decode(resp.body);
    } else {
      throw Exception('Failed to load album');
    }
  }


    @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Futurama Characters (FutureBuilder)'),
      ),
      drawer: SidebarMenu(),
      body: FutureBuilder(
          future: _fetchFuturama(),
          builder: (BuildContext context,AsyncSnapshot snapshot){
            if(snapshot.connectionState == ConnectionState.waiting){
              return Center(child: CircularProgressIndicator(),);
            }else{
              print(snapshot.data);
              var characters = snapshot.data;
              return ListView.builder(
                itemCount: characters.length,
                itemBuilder: (context, index) {
                final character = characters[index];
                return ListTile(
                    leading: Container(
                      width: 80,
                      height: 180,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(character['images']['main']),
                        ),
                      ),
                    ),
                    title: Text(character['name']['first']),
                    subtitle: Text(character['species']),
                  );
                },
              );
            }
          },
        )
    );
  }

/*
  Future<void> fetchCharacters() async {
    final response = await http.get(Uri.parse('https://api.sampleapis.com/futurama/characters'));
    if (response.statusCode == 200) {
      setState(() {
        final jsonData = json.decode(response.body);
        characters = jsonData;
      });
    } else {
      throw Exception('Failed to fetch characters');
    }
  }
*/



/*
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Futurama Characters'),
        ),
        drawer: SidebarMenu(),
        body: ListView.builder(
          itemCount: characters.length,
          itemBuilder: (context, index) {
            final character = characters[index];
            return ListTile(
              leading: Image.network(character['images']['main']),
              title: Text(character['name']['first']),
              subtitle: Text(character['species']),
            );
          },
        ),
      ),
    );
  }
  */
}
