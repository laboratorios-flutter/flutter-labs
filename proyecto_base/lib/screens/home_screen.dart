import 'package:flutter/material.dart';
import 'package:proyecto_base/screens/sidebar_menu.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:proyecto_base/usuario_provider.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  late Future<String> _username;

  void initState() {
    super.initState();
    _username = _prefs.then((SharedPreferences prefs) {
      return prefs.getString('username') ?? '';
    });
  }

  @override
  Widget build(BuildContext context) {
    final usuario = Provider.of<Usuario>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      drawer: SidebarMenu(),
      body: Center(
          child: FutureBuilder<String>(
              future: _username,
              builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
                if (snapshot.hasData) {
                  return Text(
                      'Welcome ${snapshot.data == null ? '' : snapshot.data}, ${usuario.getUsuario}' );
                } else {
                  return Text('Error: ${snapshot.error}');
                }
              })),
    );
  }
  
}
