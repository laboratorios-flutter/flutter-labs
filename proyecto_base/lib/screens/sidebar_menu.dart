import 'package:flutter/material.dart';

class SidebarMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Text(
              'Menu',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/');
            },
          ),
          ListTile(
            leading: Icon(Icons.login),
            title: Text('Sign in'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/signin');
            },
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Sign Up'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/signup');
            },
          ),
          ListTile(
            leading: Icon(Icons.people),
            title: Text('Rick and Morty'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/rick-and-morty');
            },
          ),
          ListTile(
            leading: Icon(Icons.people),
            title: Text('Rick and Morty 2'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/rick-and-morty2');
            },
          ),
          ListTile(
            leading: Icon(Icons.people),
            title: Text('Futurama'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/futurama');
            },
          ),
          ListTile(
            leading: Icon(Icons.person_pin_rounded),
            title: Text('Alumnos'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/alumnos-list');
            },
          ),
          ListTile(
            leading: Icon(Icons.person_pin_rounded),
            title: Text('Alumnos 2'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/alumnos-list2');
            },
          ),
          ListTile(
            leading: Icon(Icons.shopping_cart),
            title: Text('Shopping Cart'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/shopping-provider');
            },
          ),
          ListTile(
            leading: Icon(Icons.camera),
            title: Text('Take Picture'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/take-picture');
            },
          ),
          ListTile(
            leading: Icon(Icons.data_array),
            title: Text('Sqlite'),
            onTap: () {
              Navigator.pop(context);
              Navigator.pushReplacementNamed(context, '/sqlite');
            },
          ),
        ],
      ),
    );
  }
}
