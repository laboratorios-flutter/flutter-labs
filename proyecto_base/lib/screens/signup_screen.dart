import 'package:flutter/material.dart';
import 'package:proyecto_base/screens/sidebar_menu.dart';
import 'package:proyecto_base/widgets/form_sign_up.dart';

class SignUpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sign Up'),
      ),
      drawer: SidebarMenu(),
      body: Center(
        child: FormSignUpWidget(),
      ),
    );
  }
}
