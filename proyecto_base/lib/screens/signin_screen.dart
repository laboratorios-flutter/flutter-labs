import 'package:flutter/material.dart';

import 'package:proyecto_base/screens/sidebar_menu.dart';
import 'package:proyecto_base/widgets/form_sign_in.dart';

class SignInScreen extends StatelessWidget {
  const SignInScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sign In'),
      ),
      drawer: SidebarMenu(),
      body: Center(
        child: FormSignInWidget(),
      ),
    );
  }
}
