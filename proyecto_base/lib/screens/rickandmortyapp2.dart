import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:proyecto_base/screens/sidebar_menu.dart';

void main() {
  runApp(RickAndMortyApp2());
}

class RickAndMortyApp2 extends StatefulWidget {
  @override
  _RickAndMortyAppState createState() => _RickAndMortyAppState();
}

class _RickAndMortyAppState extends State<RickAndMortyApp2> {
  List<dynamic> characters = [];

  @override
  void initState() {
    super.initState();
    fetchCharacters();
  }

  Future<void> fetchCharacters() async {
    final response = await http.get(Uri.parse('https://rickandmortyapi.com/api/character/'));
    if (response.statusCode == 200) {

      setState(() {
        final jsonData = json.decode(response.body);
        characters = jsonData['results'];
      });
      
    } else {
      throw Exception('Failed to fetch characters');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Rick and Morty'),
      ),
      drawer: SidebarMenu(),
      body: ListView.builder(
          itemCount: characters.length,
          itemBuilder: (context, index) {
            final character = characters[index];
            return ListTile(
              leading: Image.network(character['image']),
              title: Text(character['name']),
              subtitle: Text(character['status']),
            );
          },
        ),
    );
  }

}