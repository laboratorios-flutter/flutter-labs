import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:proyecto_base/screens/sidebar_menu.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(RickAndMortyApp());
}

class RickAndMortyApp extends StatefulWidget {
  @override
  _RickAndMortyAppState createState() => _RickAndMortyAppState();
}

class _RickAndMortyAppState extends State<RickAndMortyApp> {

  @override
  void initState() {
    super.initState();
  }

Future<List> _fetchRickAndMorty() async {
    final resp = await http.get(Uri.parse('https://rickandmortyapi.com/api/character/'));
    if (resp.statusCode == 200) {
      return jsonDecode(resp.body)['results'];
    } else {
      throw Exception('Failed to load album');
    }
}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Rick and Morty (FutureBuilder)'),
      ),
      drawer: SidebarMenu(),
      body: FutureBuilder(
          future: _fetchRickAndMorty(),
          builder: (BuildContext context,AsyncSnapshot snapshot){
            if(snapshot.connectionState == ConnectionState.waiting){
              return Center(child: CircularProgressIndicator(),);
            }else{
              print(snapshot.data);
              var characters = snapshot.data;
              return ListView.builder(
                itemCount: characters.length,
                itemBuilder: (context, index) {
                final character = characters[index];
                return ListTile(
                    leading: Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(character['image']),
                        ),
                      ),
                    ),
                  title: Text(character['name']),
                  subtitle: Text(character['status']),
                  );
                },
              );
            }
          },
        )
    );
  }
  
}

