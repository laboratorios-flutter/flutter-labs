import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:proyecto_base/screens/sidebar_menu.dart';
import 'package:proyecto_base/alumnos/services/alumno.service.dart';

class CamaraApp extends StatefulWidget {
  final CameraDescription camera;

  const CamaraApp({Key? key, required this.camera}) : super(key: key);

  @override
  State<CamaraApp> createState() => _CamaraAppState();
}

class _CamaraAppState extends State<CamaraApp> {
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();
    _controller = CameraController(
      widget.camera,
      ResolutionPreset.low,
    );
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _takePicture() async {
    try {
      await _initializeControllerFuture;

      final XFile _image = await _controller.takePicture();

      ApiService.uploadImage2(_image);

      if (!mounted) return;

      print('take picture ok ');
    } catch (e) {
      print('Error capturing image: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Camera Example'),
        ),
        drawer: SidebarMenu(),
        body: FutureBuilder<void>(
          future: _initializeControllerFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return CameraPreview(_controller);
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.camera),
          onPressed: _takePicture,
        ),
      ),
    );
  }
}
