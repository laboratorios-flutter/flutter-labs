import 'package:flutter/material.dart';
import 'package:proyecto_base/provider_shopping/models/product.model.dart';

class ProductList with ChangeNotifier {
  List<Product> _products = [
    Product('Product 1', 9.99),
    Product('Product 2', 8.99),
    Product('Product 3', 7.99),
  ];

  List<Product> get getProducts => _products;
  
}
