import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:proyecto_base/provider_shopping/product_list_provider.dart';
import 'package:proyecto_base/provider_shopping/cart_provider.dart';
import 'package:proyecto_base/provider_shopping/screens/cart.dart';
import 'package:proyecto_base/screens/sidebar_menu.dart';

class Catalog extends StatelessWidget {
  const Catalog({super.key});

  @override
  Widget build(BuildContext context) {
    final productList = Provider.of<ProductList>(context);
    final cart = Provider.of<Cart>(context);

    return Scaffold(
      appBar: AppBar(title: Text('Product List'),),
      drawer: SidebarMenu(),
      body: ListView.builder(
        itemCount: productList.getProducts.length,
        itemBuilder: (context,index){
          final product = productList.getProducts[index];
          return ListTile(
            title: Text(product.name),
            subtitle: Text('\$${product.price.toStringAsFixed(2)}'),
            trailing: IconButton(
              icon: Icon(Icons.add_shopping_cart),
              onPressed: () => cart.addProduct(product),
            ),
          );
        }
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: (){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CartPage() )
          );
        }, 
        label: Text('View Cart'),
        icon : Icon(Icons.shopping_cart),
      ),
    );
  }
}