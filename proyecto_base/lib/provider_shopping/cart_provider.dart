import 'package:flutter/material.dart';
import 'package:proyecto_base/provider_shopping/models/product.model.dart';

class Cart with ChangeNotifier {
  List<Product> _products = [];

  List<Product> get products => _products;

  void addProduct(Product product) {
    _products.add(product);
    notifyListeners();
  }

  void removeProduct(int index) {
    _products.removeAt(index);
    notifyListeners();
  }

  double get totalPrice {
    double total = 0;
    for (var product in _products) {
      total += product.price;
    }
    return total;
  }
}
