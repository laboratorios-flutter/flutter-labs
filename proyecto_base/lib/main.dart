
import 'package:flutter/material.dart';
import 'package:proyecto_base/screens/signup_screen.dart';
import 'package:proyecto_base/screens/home_screen.dart';
import 'package:proyecto_base/screens/signin_screen.dart';

import 'package:proyecto_base/screens/rickandmortyapp.dart';
import 'package:proyecto_base/screens/rickandmortyapp2.dart';
import 'package:proyecto_base/screens/futurama_app.dart';

import 'package:proyecto_base/alumnos/alumnos_list.dart';
import 'package:proyecto_base/alumnos/alumnos_list2.dart';

import 'package:proyecto_base/provider_shopping/screens/catalog.dart';
import 'package:proyecto_base/provider_shopping/cart_provider.dart';
import 'package:proyecto_base/provider_shopping/product_list_provider.dart';
import 'package:proyecto_base/usuario_provider.dart';

import 'package:proyecto_base/sqlite/task_list_screen.dart';

import 'package:provider/provider.dart';

void main() {
  runApp(ProyectoBase());
}

class ProyectoBase extends StatelessWidget {
  const ProyectoBase({super.key});
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ProductList()),
        ChangeNotifierProvider(create: (context) => Cart()),
        ChangeNotifierProvider(create: (context) => Usuario()),
      ],
      child: MaterialApp(
        title: 'Proyecto Base',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        initialRoute: '/',
        routes: {
          '/': (context) => HomeScreen(),
          '/signin': (context) => SignInScreen(),
          '/signup': (context) => SignUpScreen(),
          '/rick-and-morty': (context) => RickAndMortyApp(),
          '/rick-and-morty2': (context) => RickAndMortyApp2(),
          '/futurama': (context) => FuturamaApp(),
          '/alumnos-list': (context) => AlumnosList(),
          '/alumnos-list2': (context) => AlumnosList2(),
          '/shopping-provider': (context) => Catalog(),
          '/sqlite': (context) => TaskListScreen(),
        },
      ),
    );
  }
}
